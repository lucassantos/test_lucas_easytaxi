<?php

namespace AppBundle\Service;

use Predis;

/**
* Here you have to implement a CacheService with the operations above.
* It should contain a failover, which means that if you cannot retrieve
* data you have to hit the Database.
**/
class CacheService
{
    protected $redis;
    protected $failover = false;

    /**
     * @param $host
     * @param $port
     * @param $prefix
     */
    public function __construct($host, $port, $prefix)
    {
        try {
            $this->redis = new Predis\Client("tcp://$host:$port");
            $this->redis->connect();
        } catch (Predis\Connection\ConnectionException $e) {
            $this->failover = true;
        }
    }

    /**
     * @param $key
     * @return $this|string
     */
    public function get($key)
    {
        if ($this->failover) {
           return null;
        }

        return $this->getRedisCacheByKey($key);
    }

    /**
     * @param $key
     * @return array
     */
    private function getRedisCacheByKey($key)
    {
        return $this->redis->get($key);
    }

    /**
     * @param $key
     * @param $value
     * @return $this|bool
     */
    public function set($key, $value)
    {
        if ($this->failover) {
            return null;
        }

        $this->redis->set($key,$value);
        return $this;
    }

    /**
     * @param $key
     * @return $this
     */
    public function del($key)
    {
        if ($this->failover) {
            return null;
        }

        $this->redis->del([$key]);
        return $this;
    }
}
