<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CustomersController extends Controller
{
    const TEST_CACHE_KEY = 'cache';

    /**
     * @Route("/customers/")
     * @Method("GET")
     */
    public function getAction()
    {
        $cacheService   = $this->getCacheService();
        $customers      = $this->getCacheCustomers($cacheService);

        if (empty($customers)) {
            $customers = $this->getDbCustomers();
        }

        $this->setCache($cacheService);
        return new JsonResponse(json_decode($customers));
    }

    /**
     * @Route("/customers/")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $cacheService   = $this->getCacheService();
        $database = $this->get('database_service')->getDatabase();
        $customers = json_decode($request->getContent());

        if (empty($customers)) {
            return new JsonResponse(['status' => 'No donuts for you'], 400);
        }

        foreach ($customers as $customer) {
            $database->customers->insert($customer);
        }

        $this->setCache($cacheService);
        return new JsonResponse(['status' => 'Customers successfully created']);
    }

    /**
     * @Route("/customers/")
     * @Method("DELETE")
     */
    public function deleteAction()
    {
        $cacheService   = $this->getCacheService();
        $database = $this->get('database_service')->getDatabase();
        $database->customers->drop();

        $this->getCacheService()->del(self::TEST_CACHE_KEY);
        return new JsonResponse(['status' => 'Customers successfully deleted']);
    }

    /**
     * @return object
     */
    private function getCacheService()
    {
        return $this->get('cache_service');
    }

    /**
     * @param $cacheService
     * @return mixed
     */
    private function getCacheCustomers($cacheService)
    {
        return $cacheService->get(self::TEST_CACHE_KEY);
    }

    /**
     * @param $cacheService
     */
    private function setCache($cacheService)
    {
        $cacheService->set(self::TEST_CACHE_KEY, json_encode($this->getDbCustomers()));
    }

    /**
     * @return array
     */
    private function getDbCustomers()
    {
        $database = $this->get('database_service')->getDatabase();
        $customers = $database->customers->find();
        $customers = iterator_to_array($customers);

        return $customers;
    }
}
